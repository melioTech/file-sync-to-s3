# SONY -> S3 file sync
This repository hosts a PowerShell script which enables a Windows machine to monitory a directory (and all subdirectories) and
insert an entry in the Windows Event Log when a new file is added.  This is intended to be coupled with a Task Schedulure task
which monitors the event log and fires a configured aws s3 sync command whenever there is a change.  The result being that
all the files in the path are uploade to the configured S3.

## Monitoring
Only log entries are in the Windows Even Log on the local machine
TODO: Can we emit logs to logz.io?

### Prerequisites
 - PowerShell must be available on the machine
      
## Credentials
 - The script will install a service which runs in the background as the user it's installed as
 - The user must have 

### setup
 - To install, run the powershell command as an administrator
   - Run powershell command

```
PowerShell -ExecutionPolicy Bypass -File D:\Path\To\File\UploadFilesToS3.ps1 -Setup  -Directory "D:\\Users\\fcs\\Path\\monitor"
```
 - This will add an entry to the registry with a pointer to the directory to use as a log file and configuration file.  On first run, this will output an error (it's safe to ignore)
 - Verify it's installed by
```
PowerShell -ExecutionPolicy Bypass -File D:\Path\To\File\UploadFilesToS3.ps1 -Status
```
 - Start the service by
```
PowerShell -ExecutionPolicy Bypass -File D:\Path\To\File\UploadFilesToS3.ps1 -Start
```
### Further documentation can be found here:
 - https://docs.google.com/document/d/1vOM6lLBo-AIbX5UVOaC-yECPn_MbVFnq4XjAZXAvNzs/edit
 
